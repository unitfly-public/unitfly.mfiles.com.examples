﻿using MFilesAPI;
using System.IO;

namespace Unitfly.MFiles.COM.Examples
{
    public static class CrudExamples
    {
        public static ObjectVersionAndProperties Create(Vault vault, int type, int @class, string title, params string[] paths)
        {
            var props = new PropertyValues();

            // Class
            var classProp = new PropertyValue() { PropertyDef = (int)MFBuiltInPropertyDef.MFBuiltInPropertyDefClass };
            classProp.Value.SetValue(MFDataType.MFDatatypeLookup, @class);
            props.Add(-1, classProp);

            // Name or title
            var nameProp = new PropertyValue() { PropertyDef = (int)MFBuiltInPropertyDef.MFBuiltInPropertyDefNameOrTitle };
            nameProp.Value.SetValue(MFDataType.MFDatatypeText, title);
            props.Add(-1, nameProp);

            // Define the source files to add
            var sourceFiles = new SourceObjectFiles();

            // Add one file
            foreach (var path in paths ?? new string[0])
            {
                var file = new SourceObjectFile
                {
                    SourceFilePath = path,
                    Title = Path.GetFileNameWithoutExtension(path), // For single-file-documents this is ignored
                    Extension = Path.GetExtension(path).TrimStart('.')
                };
                sourceFiles.Add(-1, file);
            }

            // SFD?
            var isSingleFileDocument = sourceFiles.Count == 1;

            // Create the object and check it in
            return vault.ObjectOperations.CreateNewObjectEx(
                type,
                props,
                sourceFiles,
                SFD: isSingleFileDocument,
                CheckIn: true);
        }

        public static ObjectVersion Update(Vault vault, int type, int id, params PropertyValue[] props)
        {
            var objID = new ObjID();
            objID.SetIDs(ObjType: type, ID: id);

            // Check out the object
            var checkedOutObjectVersion = vault.ObjectOperations.CheckOut(objID);

            // Update the property on the server
            foreach (var prop in props ?? new PropertyValue[0])
            {
                vault.ObjectPropertyOperations.SetProperty(
                    ObjVer: checkedOutObjectVersion.ObjVer,
                    PropertyValue: prop);
            }

            // Check the object back in
            return vault.ObjectOperations.CheckIn(checkedOutObjectVersion.ObjVer);
        }

        public static ObjectVersion AddFile(Vault vault, int type, int id, params string[] files)
        {
            var objID = new ObjID();
            objID.SetIDs(ObjType: type, ID: id);

            // Check out the object
            var checkedOutObjectVersion = vault.ObjectOperations.CheckOut(objID);

            // Update the property on the server
            foreach (var path in files ?? new string[0])
            {
                vault.ObjectFileOperations.AddFile(
                   ObjVer: checkedOutObjectVersion.ObjVer,
                   SourcePath: path,
                   Title: Path.GetFileNameWithoutExtension(path),
                   Extension: Path.GetExtension(path).TrimStart('.'));
            }

            // Check the object back in
            return vault.ObjectOperations.CheckIn(checkedOutObjectVersion.ObjVer);
        }

        public static ObjectVersion Delete(Vault vault, int type, int id)
        {
            var objID = new ObjID();
            objID.SetIDs(ObjType: type, ID: id);

            return vault.ObjectOperations.DeleteObject(objID);
        }

        public static void Destroy(Vault vault, int type, int id, int version = -1)
        {
            var objID = new ObjID();
            objID.SetIDs(ObjType: type, ID: id);

            vault.ObjectOperations.DestroyObject(objID, version == -1, version);
        }
    }
}
