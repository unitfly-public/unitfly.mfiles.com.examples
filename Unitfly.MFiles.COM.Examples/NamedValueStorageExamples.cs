﻿using MFilesAPI;

namespace Unitfly.MFiles.COM.Examples
{
    public static class NamedValueStorageExamples
    {
        /// <summary>
        /// Gets vault application configuration.
        /// </summary>
        /// <param name="vaultGuid">Vault GUID in curly brackets</param>
        /// <param name="vaultApplicationClassFullName">Vault application class full name 
        /// (eq. Company.Product.VaultApplication)</param>
        /// <returns></returns>
        public static string GetApplicationConfiguration(Vault vault, string vaultApplicationClassFullName)
        {
            var key = "configuration";
            var namedValues = vault.NamedValueStorageOperations.GetNamedValues(MFNamedValueType.MFSystemAdminConfiguration, vaultApplicationClassFullName);

            string config = null;
            if (namedValues.Contains(key))
            {
                config = namedValues[key] as string;
            }

            return config;
        }
    }
}
