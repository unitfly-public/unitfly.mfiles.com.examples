﻿using MFilesAPI;
using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace Unitfly.MFiles.COM.Examples
{
    class Program
    {
        private const string VaultGUID = "{F055DFCA-A641-4DF1-AF8D-A0678C83563B}";
        private const string VaultApplication = "Unitfly.ExtensionKit.VAF.VaultApplication";

        static void Main(string[] args)
        {
            var vault = LoginAsClient();

            // Named vaule storage
            {
                var config = NamedValueStorageExamples.GetApplicationConfiguration(vault, VaultApplication);
                Console.WriteLine($"{VaultApplication} config:");
                Console.WriteLine(config);

            }

            Console.WriteLine("--------------------------------------------------------------------------------");

            // Search
            {
                var results = SearchExamples.SearchForObjectsModifiedToday(vault);
                Console.WriteLine($"Objects modified today:");
                foreach (var obj in results)
                {
                    Console.WriteLine($" {obj.ObjVer.Type}-{obj.ObjVer.ID} {obj.Title}");
                }
            }

            Console.WriteLine("--------------------------------------------------------------------------------");

            // FTS
            {
                var query = "test";
                var results = SearchExamples.FullTextSearch(vault, query);
                Console.WriteLine($"Searched for '{query}':");
                foreach (var obj in results)
                {
                    Console.WriteLine($" {obj.ObjVer.Type}-{obj.ObjVer.ID} {obj.Title}");
                }
            }

            Console.WriteLine("--------------------------------------------------------------------------------");

            // CRUD
            {
                var newObj = CrudExamples.Create(vault: vault,
                    type: (int)MFBuiltInObjectType.MFBuiltInObjectTypeDocument,
                    @class: (int)MFBuiltInDocumentClass.MFBuiltInDocumentClassOtherDocument,
                    title: Guid.NewGuid().ToString(),
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files/File01.txt"),
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files/File02.txt"));

                Thread.Sleep(1000);
                var nameProp = newObj.Properties.Cast<PropertyValue>().FirstOrDefault(p => p.PropertyDef == (int)MFBuiltInPropertyDef.MFBuiltInPropertyDefNameOrTitle);
                nameProp.Value.SetValue(MFDataType.MFDatatypeText, $"Renamed {nameProp.GetValueAsLocalizedText()}");
                var updatedObj = CrudExamples.Update(vault, newObj.ObjVer.Type, newObj.ObjVer.ID, nameProp);

                Thread.Sleep(1000);
                updatedObj = CrudExamples.AddFile(vault, updatedObj.ObjVer.Type, updatedObj.ObjVer.ID, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files/File03.txt"));

                Thread.Sleep(1000);
                CrudExamples.Destroy(vault, updatedObj.ObjVer.Type, updatedObj.ObjVer.ID, 1);
            }
        }

        private static Vault LoginAsServer()
        {
            var app = new MFilesServerApplication();
            app.ConnectEx();
            return app.LogInToVaultEx(VaultGUID);
        }

        private static Vault LoginAsServerAdministrative()
        {
            var app = new MFilesServerApplication();
            app.ConnectAdministrativeEx();
            return app.LogInToVaultAdministrativeEx(VaultGUID);
        }

        private static Vault LoginAsClient()
        {
            var clientApp = new MFilesClientApplication();
            return clientApp.GetVaultConnectionsWithGUID(VaultGUID)
                .Cast<VaultConnection>()
                .FirstOrDefault()?
                .BindToVault(IntPtr.Zero, CanLogIn: true, ReturnNULLIfCancelledByUser: true);
        }
    }
}

