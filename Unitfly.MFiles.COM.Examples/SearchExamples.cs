﻿using MFilesAPI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Unitfly.MFiles.COM.Examples
{
    public static class SearchExamples
    {
        public static IEnumerable<ObjectVersion> SearchForObjectsModifiedToday(Vault vault)
        {
            var searchConditions = new SearchConditions();
            searchConditions.Add(-1, NotDeletedCondition());
            searchConditions.Add(-1, ModifiedTodayCondition());
            return SearchPaginated(vault, searchConditions);
        }

        public static IEnumerable<ObjectVersion> FullTextSearch(Vault vault, string input)
        {
            var searchConditions = new SearchConditions();
            searchConditions.Add(-1, NotDeletedCondition());
            searchConditions.Add(-1, FullTextSearch(input));
            return SearchPaginated(vault, searchConditions);
        }

        private static IEnumerable<ObjectVersion> SearchPaginated(IVault vault, SearchConditions searchConditions)
        {
            var objs = new List<ObjectVersion>();

            var segmentSize = 500;
            var segmentIdx = 0;
            var moreItems = true;
            while (moreItems)
            {
                // Get current segment
                var segment = GetSegment(vault, searchConditions, segmentIdx, segmentSize);
                objs.AddRange(segment?.ObjectVersions?.Cast<ObjectVersion>() ?? new ObjectVersion[0]);
                
                // Move to the next segment.
                segmentIdx++;

                // Check if there are more items
                moreItems = CheckIfItemsExist(vault, searchConditions, segmentIdx, segmentSize);
            }

            return objs;
        }

        private static ObjectSearchResults GetSegment(IVault vault, SearchConditions srcConds, int segmentIdx, int segmentSize)
        {
            var internalSearchConditions = srcConds.Clone();
            SearchCondition condition = new SearchCondition
            {
                ConditionType = MFConditionType.MFConditionTypeEqual
            };
            condition.Expression.SetObjectIDSegmentExpression(segmentSize);
            condition.TypedValue.SetValue(MFDataType.MFDatatypeInteger, segmentIdx);
            internalSearchConditions.Add(1, condition);
            return vault.ObjectSearchOperations
                .SearchForObjectsByConditionsEx(
                    SearchConditions: internalSearchConditions,
                    SearchFlags: MFSearchFlags.MFSearchFlagReturnLatestVisibleVersion,
                    SortResults: false,
                    MaxResultCount: 0,
                    SearchTimeoutInSeconds: 0);
        }

        private static bool CheckIfItemsExist(IVault vault, SearchConditions srcConds, int segmentIdx, int segmentSize)
        {
            // Create search condition
            var srcConditions = srcConds.Clone();
            SearchCondition srcCondition = new SearchCondition
            {
                ConditionType = MFConditionType.MFConditionTypeGreaterThanOrEqual
            };
            srcCondition.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectID, null);
            srcCondition.TypedValue.SetValue(MFDataType.MFDatatypeInteger, segmentIdx * segmentSize);
            srcConditions.Add(1, srcCondition);

            // If we get at least one item then there's more results
            return 1 == vault.ObjectSearchOperations.SearchForObjectsByConditionsEx(
                SearchConditions: srcConditions,
                SearchFlags: MFSearchFlags.MFSearchFlagReturnLatestVisibleVersion,
                SortResults: false,
                MaxResultCount: 1,
                SearchTimeoutInSeconds: 0).Count;
        }


        private static SearchCondition FullTextSearch(string input)
        {
            var condition = new SearchCondition();

            condition.Expression.SetAnyFieldExpression(
                MFFullTextSearchFlags.MFFullTextSearchFlagsLookInFileData |
                MFFullTextSearchFlags.MFFullTextSearchFlagsLookInMetaData);

            condition.ConditionType = MFConditionType.MFConditionTypeContains;

            condition.TypedValue.SetValue(MFDataType.MFDatatypeText, input);

            return condition;
        }

        private static SearchCondition NotDeletedCondition()
        {
            var condition = new SearchCondition();

            condition.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeDeleted);

            condition.ConditionType = MFConditionType.MFConditionTypeEqual;

            condition.TypedValue.SetValue(MFDataType.MFDatatypeBoolean, false);

            return condition;
        }

        private static SearchCondition ModifiedTodayCondition()
        {
            var searchCondition = new SearchCondition();

            searchCondition.Expression.SetPropertyValueExpression(
                (int)MFBuiltInPropertyDef.MFBuiltInPropertyDefLastModified,
                PCBehavior: MFParentChildBehavior.MFParentChildBehaviorNone);

            searchCondition.ConditionType = MFConditionType.MFConditionTypeGreaterThanOrEqual;

            searchCondition.TypedValue.SetValue(MFilesAPI.MFDataType.MFDatatypeTimestamp, DateTime.Today);

            return searchCondition;
        }
    }
}
